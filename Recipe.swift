//
//  Recipe.swift
//  Recipez
//
//  Created by Juan Ramirez on 5/20/16.
//  Copyright © 2016 Juan Ramirez. All rights reserved.
//

import Foundation
import CoreData
import UIKit


class Recipe: NSManagedObject {

// Insert code here to add functionality to your managed object subclass

//Used to format the data
    
    // Must save the image as data b/c databases were not meant to store big things
    func setRecipeImage(img: UIImage) {
        let data = UIImagePNGRepresentation(img)
        self.image = data       //Referencing the "image" property from "Recipe+CoreDataProperties"
    }
    
    func getRecipeImage() -> UIImage {
        let img = UIImage(data: self.image!)!
        
        return img
    }
    
}
