//
//  CreateRecipeVC.swift
//  Recipez
//
//  Created by Juan Ramirez on 5/22/16.
//  Copyright © 2016 Juan Ramirez. All rights reserved.
//

import UIKit
import CoreData

class CreateRecipeVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var recipeTitle: UITextField!
    @IBOutlet weak var recipeIngredients: UITextField!
    @IBOutlet weak var recipeSteps: UITextField!
    @IBOutlet weak var recipeImg: UIImageView!
    @IBOutlet weak var addRecipeButton: UIButton!
    
    var imagePicker: UIImagePickerController!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        recipeImg.layer.cornerRadius = 4.0
        recipeImg.clipsToBounds = true
    }

    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        
        //After finishing picking an image then dismiss the VC
        imagePicker.dismissViewControllerAnimated(true, completion: nil)
        
        //Store the selected image
        recipeImg.image = image
    }

    @IBAction func addImage (sender: AnyObject!) {
        //Load the image picker view
        presentViewController(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func createRecipe (sender: AnyObject!) {
    
        //Making sure a title exists
        if let title = recipeTitle.text where title != "" {
            
            //Need this to access coreData
            let app = UIApplication.sharedApplication().delegate as! AppDelegate
            let context = app.managedObjectContext
            
            //Must get an entity to store
            let entity = NSEntityDescription.entityForName("Recipe", inManagedObjectContext: context)!
            let recipe = Recipe(entity: entity, insertIntoManagedObjectContext: context)
            
            //Store all the values
            recipe.title = title
            recipe.ingredients = recipeIngredients.text
            recipe.steps = recipeSteps.text
            recipe.setRecipeImage(recipeImg.image!)
            
            context.insertObject(recipe)
            
            do {
                try context.save()
            } catch {
                print("Could not save recipe!")
            }
            
            self.navigationController?.popViewControllerAnimated(true)
        }
    }
}
